﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using spa.Models;

namespace spa.Controllers
{
    public class HotelsApiController : ApiController
    {
        private SamSeirIncomingEntities db = new SamSeirIncomingEntities();

        // GET: api/HotelsAPI
        public IQueryable<Hotels> GetHotels()
        {
            var hotels = db.Hotels;
            return hotels;
        }

        // GET: api/HotelsAPI/5
        [ResponseType(typeof(Hotels))]
        public IHttpActionResult GetHotels(int id)
        {
            Hotels hotels = db.Hotels.Find(id);
            if (hotels == null)
            {
                return NotFound();
            }

            return Ok(hotels);
        }

        // PUT: api/HotelsAPI/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutHotels(int id, Hotels hotels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hotels.Id)
            {
                return BadRequest();
            }

            db.Entry(hotels).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HotelsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/HotelsAPI
        [ResponseType(typeof(Hotels))]
        public IHttpActionResult PostHotels(Hotels hotels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Hotels.Add(hotels);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = hotels.Id }, hotels);
        }

        // DELETE: api/HotelsAPI/5
        [ResponseType(typeof(Hotels))]
        public IHttpActionResult DeleteHotels(int id)
        {
            Hotels hotels = db.Hotels.Find(id);
            if (hotels == null)
            {
                return NotFound();
            }

            db.Hotels.Remove(hotels);
            db.SaveChanges();

            return Ok(hotels);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HotelsExists(int id)
        {
            return db.Hotels.Count(e => e.Id == id) > 0;
        }
    }
}