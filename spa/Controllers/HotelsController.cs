﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace spa.Controllers
{
    public class HotelsController : Controller
    {
        // GET: Hotels
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddNewHotel()
        {
            return PartialView("AddHotel");
        }

        public ActionResult ShowHotels()
        {
            return PartialView("ShowAllHotels");
        }

        public ActionResult EditHotel()
        {
            return PartialView("EditHotel");
        }

        public ActionResult DeleteHotel()
        {
            return PartialView("DeleteHotel");
        }
    }
}