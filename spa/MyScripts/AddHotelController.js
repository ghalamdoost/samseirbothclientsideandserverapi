﻿app.controller('AddHotelController', function ($scope, SPACRUDService) {
    $scope.HotelId = 0;

    $scope.save = function () {
        var Hotel = {
            Id: $scope.Id,
            HotelName: $scope.HotelName,
            HotelRating: $scope.HotelRating,
            Latitude: $scope.Latitude,
            Phone: $scope.Phone,
            Address: $scope.Address,
            IsActive: $scope.IsActive,
            RegisterDate: $scope.RegisterDate
        };

        var promisePost = SPACRUDService.post(Hotel);

        promisePost.then(function (pl) {
                alert("Hotel Saved Successfully.");
            },
            function (errorPl) {
                $scope.error = 'failure loading Hotel', errorPl;
            });

    };

});  