﻿app.controller("DeleteHotelController", function ($scope, $location, ShareData, SPACRUDService) {

    getHotel();
    function getHotel() {

        var promiseGetHotel = SPACRUDService.getHotel(ShareData.value);


        promiseGetHotel.then(function (pl) {
                $scope.Hotel = pl.data;
            },
            function (errorPl) {
                $scope.error = 'failure loading Hotel', errorPl;
            });
    }

    $scope.delete = function () {
        var promiseDeleteHotel = SPACRUDService.delete(ShareData.value);

        promiseDeleteHotel.then(function (pl) {
            $location.path("/showHotels");
            },
            function (errorPl) {
                $scope.error = 'failure loading Hotel', errorPl;
            });
    };

});  