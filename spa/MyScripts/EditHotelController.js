﻿app.controller("EditHotelController", function ($scope, $location, ShareData, SPACRUDService) {
    getHotel();

    function getHotel() {
        var promiseGetHotel = SPACRUDService.getHotel(ShareData.value);

        promiseGetHotel.then(function (pl) {
                $scope.Hotel = pl.data;
            },
            function (errorPl) {
                $scope.error = 'failure loading Hotel', errorPl;
            });
    }

    $scope.save = function () {
        var Hotel = {
            Id: $scope.Id,
            HotelName: $scope.HotelName,
            HotelRating: $scope.HotelRating,
            Latitude: $scope.Latitude,
            Phone: $scope.Phone,
            Address: $scope.Address,
            IsActive: $scope.IsActive,
            RegisterDate: $scope.RegisterDate
        };

        var promisePutHotel = SPACRUDService.put($scope.Hotel.Id, Hotel);
        promisePutHotel.then(function (pl) {
            $location.path("/showHotels");
            },
            function (errorPl) {
                $scope.error = 'failure loading Hotel', errorPl;
            });
    };

});  