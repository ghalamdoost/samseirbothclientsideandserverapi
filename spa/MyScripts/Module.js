﻿var app = angular.module("ApplicationModule", ["ngRoute"]);

app.factory("ShareData", function () {
    return { value: 0 }
});

//Showing Routing  
app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    
    $routeProvider.when('/showHotels',
        {
            templateUrl: 'Hotels/ShowHotels',
            controller: 'ShowHotelsController'
        });
    $routeProvider.when('/addHotel',
        {
            templateUrl: 'Hotels/AddNewHotel',
            controller: 'AddHotelController'
        });
    $routeProvider.when("/editHotel",
        {
            templateUrl: 'Hotels/EditHotel',
            controller: 'EditHotelController'
        });
    $routeProvider.when('/deleteHotel',
        {
            templateUrl: 'Hotels/DeleteHotel',
            controller: 'DeleteHotelController'
        });
    $routeProvider.otherwise(
        {
            redirectTo: '/'
        });

    $locationProvider.html5Mode(true).hashPrefix('!');
}]);  