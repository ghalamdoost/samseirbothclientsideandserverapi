﻿app.service("SPACRUDService", function ($http) {

    //Read all Hotels  
    this.getHotels = function () {
        debugger;
        return $http.get("/api/HotelsApi");
    };

    //Function to Read hotel by HotelId  
    this.getStudent = function (id) {
        return $http.get("/api/HotelsApi/" + id);
    };

    //Function to create new Hotel  
    this.post = function (Hotel) {
        var request = $http({
            method: "post",
            url: "/api/HotelsApi",
            data: Hotel
        });
        return request;
    };

    //Edit Hotel By ID
    this.put = function (id, Hotel) {
        var request = $http({
            method: "put",
            url: "/api/HotelsApi/" + id,
            data: Hotel
        });
        return request;
    };

    //Delete Hotel By Hotel ID
    this.delete = function (id) {
        var request = $http({
            method: "delete",
            url: "/api/HotelsApi/" + id
        });
        return request;
    };
});  