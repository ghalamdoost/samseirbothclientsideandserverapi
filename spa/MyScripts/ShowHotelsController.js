﻿app.controller('ShowHotelsController', function ($scope, $location, SPACRUDService, ShareData) {
    loadAllHotelsRecords();

    function loadAllHotelsRecords() {
        var promiseGetHotel = SPACRUDService.getHotels();
        debugger;
        promiseGetHotel.then(function (pl) { $scope.Hotels = pl.data },
            function (errorPl) {
                $scope.error = errorPl;
            });
    }

    //To Edit Hotel Information  
    $scope.editHotel = function (HotelId) {
        ShareData.value = HotelId;
        $location.path("/editHotel");
    }

    //To Delete a Hotel  
    $scope.deleteHotel = function (HotelId) {
        ShareData.value = HotelId;
        $location.path("/deleteHotel");
    }
});  